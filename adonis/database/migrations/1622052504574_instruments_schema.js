'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class InstrumentsSchema extends Schema {
  up () {
    this.create('instruments', (table) => {
      table.increments()
      table.string('name');
      table.string('type');
      table.string('color');
      table.timestamps()
    })
  }

  down () {
    this.drop('instruments')
  }
}

module.exports = InstrumentsSchema
