<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class instrument extends Model
{
    protected $fillable = ['name','type','color']    ;
}
