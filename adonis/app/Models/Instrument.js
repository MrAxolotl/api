'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Instrument extends Model {
    
    instruments(){
        return this.hasMany('App/Models/Instrument')
    }
}

module.exports = Instrument
