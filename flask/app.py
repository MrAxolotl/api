from flask import Flask
from flask.globals import request
from flask.json import jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://root:ajolote2612@localhost/musicdb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False

db = SQLAlchemy(app)
ma = Marshmallow(app)

class Instruments(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.String(45), unique =True)
    type = db.Column(db.String(45))
    color = db.Column(db.String(45))
    
    def __init__(self,name,type,color):
        self.name = name
        self.type = type
        self.color = color

db.create_all()

class InstrumentSchema(ma.Schema):
    class Meta:
        fields = ("id","name","type","color")

instrument_schema = InstrumentSchema()
instruments_schema = InstrumentSchema(many=True)

@app.route('/instruments', methods=['GET'])
def getInstruments():
    allInstruments = Instruments.query.all()
    listInstruments = instruments_schema.dump(allInstruments)
    return jsonify(listInstruments)

@app.route('/instruments',methods=["POST"])
def postInstruments():
    name =  request.json['name']
    type =  request.json['type']
    color =  request.json['color']

    newInsturment = Instruments(name,type,color)
    db.session.add(newInsturment)
    db.session.commit()

    return instrument_schema.jsonify(newInsturment)
    

if __name__ == "__main__":
    app.run(debug=True,port=8000)
